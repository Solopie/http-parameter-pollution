'use strict';

// requirements
const express = require('express');
const payment = require('./payment');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });


app.get('/', (req, res) => {
    // We only allow transfer

    // Validate transfer parameter
    
    let action = req.query.action;
    let amount = req.query.amount;
    
    // Make sure the parameter is a string
    if (typeof action !== 'string') {
        throw new TypeError("action must be a string")
    }

    // Make sure the string is "transfer"
    if (action === "transfer") {
        res.send(payment(req.query.action,req.query.amount))
    } else {
        res.status(400).send('You can only transfer an amount');
    }

});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
